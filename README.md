Created this Python script to familiarize myself with the Softlayer Python library/API. There is a "createVirtualInstance" function that has not been finished yet. I do not recommend using it until further testing/completion is done. At the very moment, this script just returns account ticket information. It does utilize the argparse library so switches can be used.

ex: python sl.py -u user -k apikey
