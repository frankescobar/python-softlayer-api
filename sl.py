#!/usr/bin/python3.4
# Author: Franklin E.
# Description: A simple Python script that utilizes Softlayer's API.
# API Docs: https://sldn.softlayer.com/reference/softlayerapi
import SoftLayer
import argparse
import json

# Global variables
# ----------------
# user string variable stores Softlayer username.
# auth string variable stores the Softlayer API key.
user = ""
auth = ""

# Our Python Script Functions
# ---------------------------
# menu - outputs available options and takes input to make appropriate function calls.
def menu():
	# Status of our loop.
	status = 1
	# Construct new Softlayer object with username and api key passed as argument.	
	client = SoftLayer.Client(username=user, api_key=auth)
	#createVirtualInstance(client) // <- function not yet completed
	# Output the initial menu
	outputMenuOptions()
	# Let's start our loop to keep the program open for user input.
	while(status):
		option = input("Please enter option: ") # Ask user for menu option.
		# Quit / Exit menu/script option.
		if option.lower() == "quit" or option == "0" or option.lower() == "q":
			status = 0
		if option == "?":
			outputMenuOptions()
		if option.lower() == "fetchvirtualinstances" or option == "3":
			getVirtualInstances(client)
		if option.lower() == "fetchbaremetalservers" or option == "4":
			getBaremetalServers(client)
		# Fetch the latest open tickets option.
		if option.lower() == "fetchopentickets" or option == "2":
			recentOpenTickets(client)
		# Fetch the latest closed tickets option.
		if option.lower() == "fetchclosedtickets" or option == "1":
			recentClosedTickets(client)
		# Check the status of our loop.
		if (status != 1):
			break;
def outputMenuOptions():
    print("Welcome to SLCMD!")
    print("#################")
    print("Options Available:")
    print(" 1 | fetchClosedTickets - fetch latest closed tickets.")
    print(" 2 | fetchOpenTickets - fetch latest opened tickets.")
    print(" 3 | fetchVirtualInstances - fetch list of all virtual instances on account.")
    print(" 4 | fetchBaremetalServers - fetch list of all baremetal servers on account.")
    print(" 0 | quit - exit the slcmd script.")

# Recent Open Tickets Function
def recentOpenTickets(client, lm = 5):
	# Assign ticketsDict an array of "SoftLayer_Ticket" data type.
	# http://developer.softlayer.com/reference/datatypes/SoftLayer_Ticket
    ticketsDict = client['Account'].getOpenTickets(limit = lm) # Only return 5
    print("\nLatest Opened Tickets\n=======================")
	# Loop through array and return specified key values.
	# Key Values: id - returns ticket number, title - returns ticket title.
    for x in range(0, len(ticketsDict)):
        print("Ticket #:", ticketsDict[x]["id"], "Subject:", ticketsDict[x]["title"])
    print("=======================\n")

def recentClosedTickets(client, lm = 5):
	# Assign ticketsDict an array of "SoftLayer_Ticket" data type.
	# http://developer.softlayer.com/reference/datatypes/SoftLayer_Ticket
	ticketsDict = client['Account'].getClosedTickets(limit = lm) # Only return 5
	print("\nLatest Closed Tickets\n=======================")
	# Loop through array and return specified key values. 
	# Key Values: id - returns ticket number, title - returns ticket title.
	for x in range(0, len(ticketsDict)):
		print("Ticket #:", ticketsDict[x]["id"], "Subject:", ticketsDict[x]["title"])
	print("=======================\n")

def getVirtualInstances(client):
	virtualDict = client['Account'].getVirtualGuests()
	print("\nSoftlayer Virtual Instances\n=======================")
	for x in range(0, len(virtualDict)):
		print("Hostname:", virtualDict[x]["hostname"] + "." + virtualDict[x]["domain"], "CPU:", virtualDict[x]["maxCpu"], "Memory:",virtualDict[x]["maxMemory"] )
	print("=======================\n")

def getBaremetalServers(client):
    baremetalDict = client['Account'].getHardware()
    print("\nSoftlayer Baremetal Servers\n=======================")
    for x in range(0, len(baremetalDict)):
        print("Hostname:", baremetalDict[x]["hostname"] + "." + baremetalDict[x]["domain"])
    print("=======================\n")

def returnDataCenters(client):
	dcOptions = client['Location_Datacenter'].getViewableDatacenters(mask='name')
	print("Available Datacenters" + "\n" + "===============================" + "\n")
	for x in range(0,len(dcOptions)):
		print(dcOptions[x]['name'] + ", ", end='')
	print("\n" + "===============================" + "\n")
#def createVirtualInstance(client):
#	hostname = str(input("Please enter hostname: "))
#	domain = str(input("Please enter domain name: "))
#	startCpus = int(input("Enter number of CPU cores: "))
#	maxMemory = int(input("Enter amount of memory in megabytes: "))
#	returnDataCenters(client)
#	dataCenterName = str(input("Enter name of datacenter: "))
#	hourlyQuestion = str(input("Will this be an hourly instance? (Yes/No): "))
#	if hourlyQuestion.lower() == "yes":
#		hourlyBillingFlag = True
#	else:
#		hourlyBillingFlag = False
#	diskQuestion = str(input("Do you want local storage on virtual instance? (Yes/No)"))
#	if diskQuestion.lower() == "yes":
#		localDiskFlag = True
#	else:
#		localDiskFlag = False
#	newVirtualServer = {'hostname' : hostname, 'domain' : domain, 'startCpus' : startCpus, 'maxMemory' : maxMemory, 'datacenter.name' : dataCenterName, 'hourlyBillingFlag' : hourlyBillingFlag, 'localDiskFlag' : localDiskFlag}
#	client['Virtual_Guest'].createObject(newVirtualServer)
	
	

# This script will allow arguments to be passed down via command line by utilizing argsparse library.
parser = argparse.ArgumentParser()
parser.add_argument("-u", "--username", help="Specify the Softlayer User.")
parser.add_argument("-k", "--apikey", help="Specify the Softlayer API Key")

# args is assigned parsed variables inputed from the options above.
args = parser.parse_args()

# Check inputted arguments and update global user and auth variables to use them.
if args.username and args.apikey:
	user = args.username
	auth = args.apikey
elif args.username and args.apikey is None:
	user = args.username
	print("No api key specified!\n")
	auth = input('Please enter your API Key: ') 
elif args.username is None and args.apikey:
	auth = args.apikey
	print("No username specified!")
	user = input("Please enter your username: ")	
else:
	# If no arguments are detected, ask the user for the information.
	print("No username or API key specified through arguments!")
	user = input("Please enter your username: ")
	auth = input("Please enter your API key: ")


# Make calls to our functions
menu()




